/*!
 * jQuery Plugin: Highlight Search Terms - version 0.3
 * http://github.com/objectoriented/jquery.highlight-search-terms
 * Highlight search terms in referrer URL from Google, Yahoo!, Bing and custom site.
 *
 * Copyright (c) 2009 Kyo Nagashima <kyo@hail2u.net>
 * This library licensed under MIT license:
 * http://opensource.org/licenses/mit-license.php
 */
(function ($) {
  $.fn.highlightSearchTerms = function (options) {
    var o = $.extend({}, $.fn.highlightSearchTerms.defaults, options);

    //$.merge(o.referrerPatterns, $.fn.highlightSearchTerms.builtinReferrerPatterns);
    var ref = o.referrer || document.referrer;
    if (ref) {
      var searchTerms = extractSearchTerms(ref, o);
      // Replace search terms
      // Mimic the \b modifier where "most" of the word-boundary characters
      // are listed. This is needed to overcome situation with
      // danish (or cyrillic) characters in the term, which are considered
      // non-word characters in javascript regex.
      // See: http://breakthebit.org/post/3446894238/word-boundaries-in-javascripts-regular
      var stopWordRegex = new RegExp("(^|[\\s\\n\\r\\t.,'\\\"\\+!?-]+)(" + o.stopWords.join("|") + "\\|?)([\\s\\n\\r\\t.,'\\\"\\+!?-]+|$)", "gi");

      var highlightedTerms = searchTerms.replace(stopWordRegex, "|").replace(/[\\|]+/g, '|').replace(/\s/g, '|').replace(/^[\\|]/, '').replace(/[\\|]$/, '');

      // Highlight terms
      if (highlightedTerms !== "") {
        $.each(o.area, function (index, area) {
          $(area).highlight(highlightedTerms.split('|'), { wordsOnly: drupalSettings.highlight.wordsonly, 'className': o.className});
        });
      }
    }

    return this;
  };

  // Private: Extract terms from referrer
  function extractSearchTerms (ref, o) {
    var terms = "";

    $.each(o.referrerPatterns, function () {
      var pattern = new RegExp(this, "i");

      if (pattern.exec(ref)) {
        var unsafe = new RegExp(o.unsafeChars, "g");
        terms = decodeURIComponent(RegExp.$1).replace(unsafe, "+").replace(/^\+*(.*?)\+*$/, "$1").replace(/\++/g, "|");

        return false; // break $.each
      }
    });

    return terms;
  }

  // Private: Encode entities
  function encodeEntities (s) {
    return $("<u/>").text(s).html(); // jQuery magic
  }

  // Public: default options
  $.fn.highlightSearchTerms.defaults = {
    referrer:         '',
    className:        drupalSettings.highlight.class,
    referrerPatterns: [],
    unsafeChars:      "[!-*,-/:-@[-`{-~]",
    stopWords:        drupalSettings.highlight.stopwords.split(',').map(s => s.trim()),
    area:             drupalSettings.highlight.area.split('\n').map(s => s.trim())
  };

  // Public: built-in referrer patterns for Google(com|co.jp), Yahoo!(com|co.jp), Bing.
  //$.fn.highlightSearchTerms.builtinReferrerPatterns = [];

  Drupal.behaviors.highlight = {
    'attach': function(context, settings) {
      $.fn.highlightSearchTerms({referrerPatterns: drupalSettings.highlight.referrerPatterns.split(/\r\n|\r|\n/)});
      $.fn.highlightSearchTerms({referrerPatterns: drupalSettings.highlight.patterns.split(/\r\n|\r|\n/), referrer: document.URL});
      var styles = {
        'background-color': drupalSettings.highlight.color,
        'color': drupalSettings.highlight.textColor
      };
      $('.' + drupalSettings.highlight.class).css(styles);
    }
  };
})(jQuery);

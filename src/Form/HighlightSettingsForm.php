<?php

namespace Drupal\highlight\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure highlight settings for this site.
 */
class HighlightSettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'highlight.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'highlight_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['area'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Highlight area'),
      '#default_value' => $config->get('area'),
      '#description' => $this->t('Use jQuery selector to set the part of HTML you want to highlight.'),
    ];
    $form['class'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Highlight CSS class'),
      '#default_value' => $config->get('class'),
      '#description' => $this->t('Set the highlight CSS class that will be used for the enclosing highlight HTML tag.'),
    ];
    $form['color'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Highlight color'),
      '#default_value' => $config->get('color'),
      '#description' => $this->t('Set the highlight color with CSS value.'),
    ];
    $form['text_color'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Highlighted text color'),
      '#default_value' => $config->get('text_color'),
      '#description' => $this->t('Set the highlight color with CSS value.'),
    ];
    $form['wordsonly'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Highlight words'),
      '#default_value' => $config->get('wordsonly'),
      '#description' => $this->t('Highlight only full words.'),
    ];
    $form['patterns'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Local search patterns'),
      '#default_value' => implode("\n", $config->get('patterns')),
      '#description' => $this->t('Set the patterns to get local search keywords to highlight. Use JavaScript Regex and put each parttern per line.'),
    ];

    $form['patterns_referrer'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Referrer patterns'),
      '#default_value' => implode("\n", $config->get('patterns_referrer')),
      '#description' => $this->t('Set the referrer patterns for search engines. Use JavaScript Regex and put each parttern per line.'),
    ];
    $form['stopwords'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Stopwords'),
      '#default_value' => $config->get('stopwords'),
      '#description' => $this->t("Stopwords which shouldn't be highlighted."),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->configFactory->getEditable(static::SETTINGS)
      ->set('area', $form_state->getValue('area'))
      ->set('class', $form_state->getValue('class'))
      ->set('color', $form_state->getValue('color'))
      ->set('text_color', $form_state->getValue('text_color'))
      ->set('wordsonly', $form_state->getValue('wordsonly'))
      ->set('patterns', explode("\n", $form_state->getValue('patterns')))
      ->set('patterns_referrer', explode("\n", $form_state->getValue('patterns_referrer')))
      ->set('stopwords', $form_state->getValue('stopwords'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
